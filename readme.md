In this, we read the temperature available in degree Celsius in Kafka Topic `mqtt.temeprature` and convert the temperature to degree Fahrenheit and push it to another Kafka topic `temperatureUS`.

	1. Create a output Kafka topic `temperatureUS` using following command :
        kafka-topics --zookeeper localhost:2181 --create --topic temperatureUS --partitions 1 --replication-factor 1

	2. Git clone https://github.com/kwartile/kafka-streams-processing-example-1.git
	    Note : You would need to update the broker host and port,in com.kwartile.iot.streams.example.DegreeCelsiusToFahrenheitConverter class, to the respective host and port in your environment.

	3. Create the jar file using `mvn package`

    4. Run the Kafka Streams application using `java -jar target/kwartile-kafka-streams-1.0-SNAPSHOT-jar-with-dependencies.jar`

This should start converting the temperature in topic `mqtt.temperature` (which is in degree Celsius) to degree Fahrenheit and push it onto another topic`temperatureUS`.
