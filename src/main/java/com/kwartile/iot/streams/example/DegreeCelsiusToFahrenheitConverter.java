package com.kwartile.iot.streams.example;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.KStream;

import java.util.Properties;

/**
 * This is a stream processor that converts the input temperature reading from degree Celsius to degree Fahrenheit.
 */

public class DegreeCelsiusToFahrenheitConverter {

    public static void main(String[] args) throws Exception {

        Properties streamsConfiguration = new Properties();

        //Kafka Streams application name
        streamsConfiguration.put(StreamsConfig.APPLICATION_ID_CONFIG, "kwartile-streams-processing-example-1");

        //Specify the cluster address of the Brokers in Kafka cluster
        streamsConfiguration.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");

        //Specify the key serde. Here we have specified Bytes since the keys are nulls!
        streamsConfiguration.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.ByteArray().getClass().getName());

        //Specify the value serde.
        streamsConfiguration.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());

        StreamsBuilder builder = new StreamsBuilder();

        //Read the input Kafka Topic into a Kafka Stream instance.
        KStream<byte[], String> temperatureStream = builder.stream("mqtt.temperature");

        //Apply transformation on the input value.
        KStream<byte[], String> degreeFahrenheitTemp =
                temperatureStream.mapValues(DegreeCelsiusToFahrenheitConverter::celsiusToFahrenheit);

        //Write the transformed value into another output Kafka Topic.
        degreeFahrenheitTemp.to("temperatureUS");

        KafkaStreams streams = new KafkaStreams(builder.build(), streamsConfiguration);
        streams.start();

        //Add shutdown hook to respond to SIGTERM and gracefully close the Kafka Streams.
        Runtime.getRuntime().addShutdownHook(new Thread(streams::close));
    }

    /*
    *
    * This method converts a temperature in degree Celsius to degree Fahrenheit. It returns the degree Fahrenheit value as a String.
    *
    * @param    degreeCelsius the temperature in degree Celsius.
    * @return   temperature in degree Fahrenteit
    *
     */
    public static String celsiusToFahrenheit(String degreeCelsius) {

        if(degreeCelsius == null || "".equals(degreeCelsius)){
            return "-";
        }
        try {
            return String.valueOf((Double.valueOf(degreeCelsius) * 9L / 5) + 32);
        }catch(NumberFormatException ex) {
            return "-";
        }
    }



}
