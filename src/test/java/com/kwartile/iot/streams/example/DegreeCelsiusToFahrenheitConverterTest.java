package com.kwartile.iot.streams.example;

import org.junit.Test;

import static org.junit.Assert.*;

public class DegreeCelsiusToFahrenheitConverterTest {

    @Test
    public void celsiusToFahrenheitNullValue() {
        String returnVal = DegreeCelsiusToFahrenheitConverter.celsiusToFahrenheit(null);
        assertEquals("-",returnVal);
    }

    @Test
    public void celsiusToFahrenheitInValidValue() {
        String returnVal = DegreeCelsiusToFahrenheitConverter.celsiusToFahrenheit("invalid temperature");
        assertEquals("-",returnVal);
    }

    @Test
    public void celsiusToFahrenheitValidValue() {
        String returnVal = DegreeCelsiusToFahrenheitConverter.celsiusToFahrenheit("21.0");
        assertEquals("69.8",returnVal);
    }

    @Test
    public void celsiusToFahrenheitEmptyValue() {
        String returnVal = DegreeCelsiusToFahrenheitConverter.celsiusToFahrenheit("");
        assertEquals("-",returnVal);
    }
}